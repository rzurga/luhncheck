import sys

def sumdigits(n):
    if isinstance(n, int):
        return sumdigits(str(n))
    elif isinstance(n, basestring):
        sum_digits = sum(int(x) for x in list(n))
        if sum_digits < 10:
            return sum_digits
        else:
            return sumdigits(str(sum_digits))
    else:
        raise Exception('Bad Type: {}'.format(type(n)))

def luhnck(num):
    if num is None:
        return False
    if isinstance(num, int):
        return luhnck(str(num))
    a = sum([int(x) for x in num[::-2]])
    b = sum([sumdigits(2*int(x)) for x in num[-2::-2]])
    c = (a + b) * 9
    if c == 0:      # If empty or all zero input
        return False
    luhndig = c % 10
    return luhndig == 0

def main(args):
    for n in args:
        if n.isdigit():
            print "Luhn check for {}: {}".format(n, luhnck(n))
        else:
            print "Not a number: {}".format(n)

if __name__ == "__main__":
    main(sys.argv[1:])