import unittest
from luhn.luhnck import sumdigits, luhnck


class LuhnTest(unittest.TestCase):

    def test(self):
        self.failUnless(True)

    def test_sumdigits(self):
        self.failUnlessEqual(sumdigits(0), 0)
        self.failUnlessEqual(sumdigits(1), 1)
        self.failUnlessEqual(sumdigits(9), 9)
        self.failUnlessEqual(sumdigits(12), 3)
        self.failUnlessEqual(sumdigits(123), 6)
        self.failUnlessEqual(sumdigits(123456789), 9)

    def test_lunck(self):
        self.failIf(luhnck(None))
        self.failIf(luhnck(''))
        self.failIf(luhnck(0))
        self.failUnless(luhnck(18))
        self.failIf(luhnck('1231'))
        self.failUnless(luhnck('1230'))
        self.failUnless(luhnck('12345678903'))
        self.failUnless(luhnck('4433785000281233'))
        self.failUnless(luhnck('342873628374628374628734624737'))


if __name__ == '__main__':
    unittest.main()